package com.npci.bankproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.npci.bankproject.entity.Customers;
import com.npci.bankproject.entity.TransactionEntity;
import com.npci.bankproject.service.CustomerService;
import com.npci.bankproject.service.TransactionService;

@RestController
@RequestMapping("/api/customers")

public class CustomerController {
	
	@Autowired 
	private CustomerService customerService;
	
	@Autowired 
	private TransactionService transactionService;
	
	@GetMapping("/getallcustomers")
	public ResponseEntity<List<Customers>> getAllCustomers(){
		List<Customers> customers =null;
		
		try {
			customers = customerService.getAllCustomers();
			
		}catch(Exception ex) {
			ex.getMessage();
		}
		return new  ResponseEntity<List<Customers>>(customers,HttpStatus.OK);
	}
	
//	@GetMapping("/getbyid/{id}")
//	public ResponseEntity<Customers> getById(@PathVariable ("id")int customerId){
//		Customers customers=null;
//		try {
//			customers=customerService.getById(customerId);
//		}catch(Exception e) {
//			e.getMessage();
//		}
//		return new ResponseEntity<Customers>(customers,HttpStatus.OK);
//		
//	}
	
	@GetMapping("/getbyid/{id}")
	public Customers getById(@PathVariable ("id")int customerId) throws Exception{
		Customers customers=null;
					customers=customerService.getById(customerId);
		
				return customers;
		
	}

	
//	@PostMapping ("/addorupdate") 
//	public ResponseEntity<Customers> addOrUpdate(@RequestBody Customers customer){
//		
//		Customers customers = null;
//		
//		try {
//			customers = customerService.addOrUpdate(customer);
//			
//		} catch (Exception ex) {
//			ex.getMessage();
//		}
//		
//		return new ResponseEntity<Customers>(customers, HttpStatus.ACCEPTED);
//	}

	
	@PostMapping ("/addcustomer") 
	public ResponseEntity<Customers> addcustomer(@RequestBody Customers customers){
		
		Customers customer = null;
		
		try {
			customer = customerService.addcustomer(customers);
			
		} catch (Exception ex) {
			ex.getMessage();
		}
		
		return new ResponseEntity<Customers>(customer, HttpStatus.OK);
	}
	
	
	@DeleteMapping("/deletecustomer/{id}")
    public ResponseEntity<Customers> deletecustomer(@PathVariable("id") int customerId) {
		Customers users = null;

		try {
            users = customerService.deletecustomer(customerId);
        } catch (Exception ex) {
            ex.getMessage();
        }
        return new ResponseEntity<Customers>(users, HttpStatus.OK);
    }
	
	
	@GetMapping("/alltransaction")
	public ResponseEntity<List<TransactionEntity>> getalltransaction(){
		List<TransactionEntity> trans =null;
		
		try {
			trans = transactionService.getalltransaction();
			
		}catch(Exception ex) {
			//ex.getMessage();
		}
		return new  ResponseEntity<List<TransactionEntity>>(trans,HttpStatus.OK);
	}
	
	@PostMapping("/addtransaction/")
	public ResponseEntity<TransactionEntity> addTransaction( @RequestBody TransactionEntity transactions) {
		TransactionEntity transaction = null;

		try 
		{
		transaction = transactionService.addTransaction(transactions);
		} catch (Exception ex) {
			ex.getMessage();
		}
		return new ResponseEntity<TransactionEntity>(transaction, HttpStatus.OK);
	}




	



}
